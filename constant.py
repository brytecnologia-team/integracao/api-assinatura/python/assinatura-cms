URL_INICIALIZATION = 'https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/initialize';
URL_FINALIZATION = 'https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/finalize';

AUTH_TOKEN = 'insert-a-valid-token'

FILE_PATH = "./files-toign/arquivo-assinar_hash-para-assinar.txt"
CERT_PATH = './caminho/para/o/certificado/'
CERT_PASSWORD = 'senha do certificado'

ATTACHED = 'true'
PROFILE = 'BASIC'
HASH_ALGORITHM = 'SHA1'
OPERATION_TYPE = 'SIGNATURE'
