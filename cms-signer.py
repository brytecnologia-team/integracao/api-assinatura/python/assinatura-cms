import requests
import json
from constant import URL_INICIALIZATION, URL_FINALIZATION, AUTH_TOKEN, FILE_PATH, CERT_PATH, CERT_PASSWORD, ATTACHED, PROFILE, HASH_ALGORITHM, OPERATION_TYPE
from OpenSSL import crypto
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA512, SHA256, SHA
from base64 import b64encode, b64decode


HEADER = {'Authorization': 'Bearer ' + AUTH_TOKEN}
file = open(FILE_PATH, 'rb').read()

def token_verify():
    aux_authorization = HEADER['Authorization'].split(' ')
    if(aux_authorization[1] == 'insert-a-valid-token'):
        print("Configure a valid token")
        return False
    else:
        return True

def initialize_singature():
    
    print("================ Initializing CMS Signature ... ================")
    print("")

    
    original_documents = []
    original_documents.append(('originalDocuments[0][content]', file))

    # Step 1: Load private key and digital certificate content.

    p12 = crypto.load_pkcs12(open(CERT_PATH, 'rb').read(), CERT_PASSWORD)
    cert = p12.get_certificate()
   
    cert_base_64 = crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode('utf-8').replace('-----BEGIN CERTIFICATE-----', '').replace('-----END CERTIFICATE-----', '').replace('\n', '')

    inicialization_form = {

        'nonce': 1, 
        'attached': ATTACHED,
        'profile': PROFILE,
        'hashAlgorithm': HASH_ALGORITHM,
        'certificate': cert_base_64,
        'operationType': OPERATION_TYPE,
        'originalDocuments[0][nonce]': '1',
    }

    # Step 2: Initialization of the signature and production of the signedAttributes artifacts.

    response = requests.post(URL_INICIALIZATION, data = inicialization_form, headers=HEADER, files=original_documents)
    if response.status_code == 200:

        data = response.json()
        print("Inicialization JSON response: ", data)
        print("")

        signed_attributes = data['signedAttributes']

        signed_attributes_content_decoded = b64decode(data['signedAttributes'][0]['content'])

        encrypted_message_digest_b64 = encrypt_initialized_signature(signed_attributes_content_decoded, p12.get_privatekey())

        finalize_signature(signed_attributes, encrypted_message_digest_b64, cert_base_64)

    else:
        print(response.text)


def encrypt_initialized_signature(signed_attributes_content_decoded, private_key) :

    # # Step 3: Encryption of data initialized with the certificate stored on disk.

    print("================  Initializing encrypt process  ================")
    print("")

    key = crypto.dump_privatekey(crypto.FILETYPE_PEM, private_key).decode("utf-8")

    rsa_key = RSA.importKey(key)
        
    if HASH_ALGORITHM == 'SHA1':
       hash_object = SHA.new(signed_attributes_content_decoded)
    elif HASH_ALGORITHM == 'SHA256':
        hash_object = SHA256.new(signed_attributes_content_decoded)
    elif HASH_ALGORITHM == 'SHA512':
        hash_object = SHA512.new(signed_attributes_content_decoded)
    else:
        print("invalid signature hash algorithm, allowed: 'SHA1, SHA256, SHA512' ")

    signature = PKCS1_v1_5.new(rsa_key).sign(hash_object)

    print("Encrypted Base64 data ", b64encode(signature).decode('utf-8'))
    print("")

    return b64encode(signature) 


def finalize_signature(signed_attributes, encrypted_b64_message_digest, cert_base_64):

    # Step 4: Finalization of the signature and obtaining the signed artifact.

    print("================ Finishing CMS Signature ... ================")
    print("")

    form_finalization = {
        'nonce': 1, 
        'attached': ATTACHED,
        'profile': PROFILE,
        'hashAlgorithm': HASH_ALGORITHM,
        'certificate': cert_base_64,
        'operationType': OPERATION_TYPE
        }

    for i in range(len(signed_attributes)):

        form_finalization['finalizations[0][nonce]'] = signed_attributes[i]['nonce']
        form_finalization['finalizations[0][signedAttributes]'] = signed_attributes[i]['content']
        form_finalization['finalizations[0][signatureValue]'] = encrypted_b64_message_digest
            
    documents = []
    documents.append(('finalizations[0][document]', file))

    response = requests.post(URL_FINALIZATION, data = form_finalization, headers=HEADER, files=documents)
    if response.status_code == 200:

        data = response.json()
        print("Finalization JSON response: ", data)
        print("")

        signatures_array = data['signatures']
        content_of_signature = signatures_array[0]['content']
        byte_array = b64decode(content_of_signature.encode("utf-8"))
        new_file = open("AssinaturaCMS"+".p7s", 'wb')
        new_file.write(byte_array)
        new_file.close()

        print("Signature successful and stored in the local folder as 'AssinaturaCMS.p7s' ")

    else:
        print(response.text)

if(token_verify()):
    initialize_singature()
